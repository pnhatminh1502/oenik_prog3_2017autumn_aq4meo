﻿// <copyright file="GameScreen.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace GuitarHeroClone
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Media;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;

    /// <summary>
    /// Display the GameModel and take Game logic into action
    /// </summary>
    public class GameScreen : FrameworkElement
    {
        private SoundPlayer player;
        private GameLogic logic;
        private GameModel model;
        private DispatcherTimer timer;
        private Typeface font = new Typeface("Arial");
        private Point multiplyLocation = new Point(10, 10);
        private Point streakLocation = new Point(10, 50);
        private Point scoreLocation = new Point(10, 100);

        /// <summary>
        /// Initializes a new instance of the <see cref="GameScreen"/> class.
        /// </summary>
        public GameScreen()
        {
            this.Loaded += this.GameScreen_Loaded;
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.model != null)
            {
                foreach (StaticGem gem in this.model.StaticGems)
                {
                    drawingContext.DrawGeometry(gem.GetColor(), gem.GetPen(), gem.Geometry);
                }

                if (this.model.MovingGems != null)
                {
                    foreach (MovingGem gem in this.model.MovingGems)
                    {
                        drawingContext.DrawGeometry(gem.GetColor(), new Pen(gem.GetColor(), 1), gem.Geometry);
                    }
                }

                if (this.model.SustainedGems != null)
                {
                    foreach (SustainedGem gem in this.model.SustainedGems)
                    {
                        drawingContext.DrawGeometry(gem.GetColor(), new Pen(gem.GetColor(), 1), gem.Geometry);
                    }
                }

                FormattedText score = new FormattedText("Your score: " + this.model.Score, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, this.font, 15, Brushes.Black);
                FormattedText multiply = new FormattedText("Your multiply: x" + this.model.Multiply, CultureInfo.CurrentCulture,FlowDirection.LeftToRight, this.font, 15, Brushes.Black);
                FormattedText streak = new FormattedText("Your streak: " + this.model.Streak, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, this.font, 15, Brushes.Black);
                drawingContext.DrawText(score, this.scoreLocation);
                drawingContext.DrawText(multiply, this.multiplyLocation);
                drawingContext.DrawText(streak, this.streakLocation);
            }
        }

        private void GameScreen_Loaded(object sender, RoutedEventArgs e)
        {
            this.player = new SoundPlayer("highway-to-hell.wav");
            this.player.Load();
            this.player.Play();
            this.model = new GameModel(Config.Width, Config.Height);
            this.logic = new GameLogic(this.model);
            string[] lines = File.ReadAllLines("song.txt");
            if (lines != null)
            {
                for (int y = 0; y < lines.Length; y++)
                {
                    for (int x = 0; x < lines[y].Length; x++)
                    {
                        if (lines[y][x] == 'X')
                        {
                            this.model.AddGem(GemType.MovingGem, 200 + (x * Config.Gap), (Config.Height / 2) + 50 - (y * Config.Gap));
                        }
                        else if (lines[y][x] == '|')
                        {
                            this.model.AddGem(GemType.SustainedGem, 200 + (x * Config.Gap), (Config.Height / 2) + 50 - (y * Config.Gap));
                        }
                    }
                }
            }

            this.InvalidateVisual();
            Window win = Window.GetWindow(this);
            if (win != null)
            {
                win.KeyDown += this.Win_KeyDown;
                win.KeyUp += this.Win_KeyUp;
                this.timer = new DispatcherTimer
                {
                    Interval = TimeSpan.FromMilliseconds(30)
                };
                this.timer.Tick += this.Timer_Tick;
                this.timer.Start();
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            this.logic.GameTick();
            this.InvalidateVisual();
        }

        private void Win_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Z: this.model.StaticGems[0].KeyDown = false; break;
                case Key.X: this.model.StaticGems[1].KeyDown = false; break;
                case Key.C: this.model.StaticGems[2].KeyDown = false; break;
                case Key.B: this.model.StaticGems[3].KeyDown = false; break;
                case Key.N: this.model.StaticGems[4].KeyDown = false; break;
                case Key.M: this.model.StaticGems[5].KeyDown = false; break;
            }

            this.InvalidateVisual();
        }

        private void Win_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Z: this.model.StaticGems[0].KeyDown = true; break;
                case Key.X: this.model.StaticGems[1].KeyDown = true; break;
                case Key.C: this.model.StaticGems[2].KeyDown = true; break;
                case Key.B: this.model.StaticGems[3].KeyDown = true; break;
                case Key.N: this.model.StaticGems[4].KeyDown = true; break;
                case Key.M: this.model.StaticGems[5].KeyDown = true; break;
                case Key.Space: this.timer.IsEnabled = !this.timer.IsEnabled; break;
            }

            this.InvalidateVisual();
        }
    }
}
