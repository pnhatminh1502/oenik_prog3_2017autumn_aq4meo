﻿// <copyright file="Gem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace GuitarHeroClone
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Gems are the main component of the game. It's the indicated note of the song that you have to press in order to get score
    /// There're 3 type of Gem.
    /// Each gem has: + A Point for location on the game field,
    ///               + A radius to form the shape of each gem
    ///               + A boolean indicate whether the StaticGem is pressed down \
    ///                 or whether MovingGem and SustainedGem has been pressed down when it collides with the StaticGem
    ///               + And a DY vector indicate how it'd move vertically everytick
    ///               + A GetColor() method to return the Brush color of the gem according to its X coordinator
    /// </summary>
    public class Gem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Gem"/> class.
        /// </summary>
        /// <param name="newx">X coordinate of the gem</param>
        /// <param name="newy">Y coordinate of the gem</param>
        public Gem(double newx,double newy)
        {
            this.Point = new Point(newx, newy);
            this.KeyDown = false;
            this.Radius = 20;
        }

        /// <summary>
        /// Gets or sets type of the gem. 3 types of gem: StaticGem, MovingGem and SustainedGem
        /// </summary>
        public GemType Type { get; set; }

        /// <summary>
        /// Gets or sets Point of the gem. It indicates the location of the center of the gem, or the coordinate of the Rectangle
        /// </summary>
        public Point Point { get; set; }

        /// <summary>
        /// Gets size of the gem
        /// </summary>
        public double Radius { get; }

        /// <summary>
        /// Gets or sets vertical vector of the gem
        /// </summary>
        public double DY { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether gets or sets boolean that indicate whether the gem is pressed
        /// </summary>
        public bool KeyDown { get; set; }

        /// <summary>
        /// Color of the game according to its X and Y coordinate
        /// </summary>
        /// <returns>
        /// Its color might be ( based on the X coordinate from left to right on the game field)
        /// Red - Green - Blue - Yellow - Purple - Orange
        /// Otherwise it returns no color
        /// </returns>
        public Brush GetColor()
        {
            if (this.Point.Y < Config.Height)
            {
                if (this.Point.X == 200)
                {
                    return Brushes.Red;
                }

                if (this.Point.X == 200 + Config.Gap)
                {
                    return Brushes.Green;
                }

                if (this.Point.X == 200 + (2 * Config.Gap))
                {
                    return Brushes.Blue;
                }

                if (this.Point.X == 200 + (3 * Config.Gap))
                {
                    return Brushes.Yellow;
                }

                if (this.Point.X == 200 + (4 * Config.Gap))
                {
                    return Brushes.Purple;
                }

                if (this.Point.X == 200 + (5 * Config.Gap))
                {
                    return Brushes.Orange;
                }
            }

            return null;
        }
    }
}
