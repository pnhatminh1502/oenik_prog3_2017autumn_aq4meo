﻿// <copyright file="Config.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace GuitarHeroClone
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;

    /// <summary>
    /// Configuration attribute of the game
    /// </summary>
    public class Config
    {
        /// <summary>
        /// Width of the game's field
        /// </summary>
        public const double Width = 1000;

        /// <summary>
        /// Height of the game's field
        /// </summary>
        public const double Height = 600;

        /// <summary>
        /// Horizontal alligned  Gap between 2 gems
        /// </summary>
        public const double Gap = 70;
    }
}
