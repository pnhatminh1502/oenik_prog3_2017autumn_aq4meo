﻿// <copyright file="GameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace GuitarHeroClone
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;

    /// <summary>
    /// Logic of the game. Define when static gem collides with the other type of gem and how MovingGem or SustainedGem move each tick.
    /// Also define when you get the score, when the streak and multiply increases
    /// </summary>
    public class GameLogic
    {
        private GameModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// Constructor of the GameLogic
        /// </summary>
        /// <param name="model">GameModel we're going to use</param>
        public GameLogic(GameModel model)
        {
            this.model = model;
        }

        /// <summary>
        /// Gets or sets public instance of GameModel for further use
        /// </summary>
        public GameModel Model { get => this.model; set => this.model = value; }

        /// <summary>
        /// detect the collision of StaticGem and MovingGem
        /// </summary>
        /// <param name="sg">Static Gem</param>
        /// <param name="mg">Moving Gem</param>
        /// <returns>true if collided,otherwise false</returns>
        public bool Collision1(StaticGem sg,MovingGem mg)
        {
            return sg.Geometry.FillContainsWithDetail(mg.Geometry) == IntersectionDetail.Intersects;
        }

        /// <summary>
        /// detect the collision of StaticGem and SustainedGem
        /// </summary>
        /// <param name="sg">Static Gem</param>
        /// <param name="stg">Sustained Gem</param>
        /// <returns>true if collided,otherwise false</returns>
        public bool Collision2(StaticGem sg,SustainedGem stg)
        {
            return sg.Geometry.FillContainsWithDetail(stg.Geometry) == IntersectionDetail.Intersects;
        }

        /// <summary>
        /// Move the MovingGem and SustainedGem every timer tick
        /// </summary>
        public void GameTick()
        {
            foreach (StaticGem sg in this.model.StaticGems)
            {
                if (this.model.MovingGems != null || this.Model.SustainedGems != null)
                {
                    foreach (MovingGem mg in this.Model.MovingGems)
                    {
                        mg.ChangeY();
                        if (this.Collision1(sg, mg))
                        {
                            if (sg.KeyDown)
                            {
                                mg.KeyDown = true;
                                this.Model.Score++;
                                this.Model.Streak++;
                                if (this.Model.Streak % 200 == 0)
                                {
                                    this.Model.Multiply++;
                                }
                            }
                        }

                        if (mg.Geometry.Center.Y > sg.Geometry.Center.Y + mg.Geometry.RadiusY && !mg.KeyDown)
                        {
                            this.Model.Streak = 0;
                            this.Model.Multiply = 1;
                        }
                    }

                    foreach (SustainedGem stg in this.Model.SustainedGems)
                    {
                        stg.ChangeY();
                        if (this.Collision2(sg, stg))
                        {
                            if (sg.KeyDown)
                            {
                                stg.KeyDown = true;
                                this.Model.Score++;
                                this.Model.Streak++;
                                if (this.Model.Streak % 200 == 0)
                                {
                                    this.Model.Multiply++;
                                }
                            }

                            if (stg.Geometry.Rect.Y > stg.Geometry.Rect.Height && !stg.KeyDown)
                            {
                                this.Model.Streak = 0;
                                this.Model.Multiply = 1;
                            }
                        }
                    }
                }
            }
        }
    }
}
