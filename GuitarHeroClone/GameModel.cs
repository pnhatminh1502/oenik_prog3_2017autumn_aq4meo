﻿// <copyright file="GameModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace GuitarHeroClone
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;

    /// <summary>
    /// All models which are going to be used
    /// </summary>
    public class GameModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameModel"/> class.
        /// </summary>
        /// <param name="w">Width of game's field</param>
        /// <param name="h">Height of game's field</param>
        public GameModel(double w, double h)
        {
            this.StaticGems = new StaticGem[6];
            this.MovingGems = new List<MovingGem>();
            this.SustainedGems = new List<SustainedGem>();
            this.Score = 0;
            this.Multiply = 1;
            this.Streak = 0;
            for (int i = 0; i < this.StaticGems.Length; i++)
            {
                this.StaticGems[i] = new StaticGem(200 + (Config.Gap * i), Config.Height - Config.Gap);
            }
        }

        /// <summary>
        /// Gets or sets of the Array length of 6 of StaticGems
        /// </summary>
        public StaticGem[] StaticGems { get; set; }

        /// <summary>
        /// Gets or sets generic list of MovingGem
        /// </summary>
        public List<MovingGem> MovingGems { get; set; }

        /// <summary>
        /// Gets or sets generic list of SustainedGem
        /// </summary>
        public List<SustainedGem> SustainedGems { get; set; }

        /// <summary>
        /// Gets or sets score of the game
        /// </summary>
        public int Score { get; set; }

        /// <summary>
        /// Gets or sets multiply attributes of the game
        /// </summary>
        public int Multiply { get; set; }

        /// <summary>
        /// Gets or sets number of streaks of correct gem hit. It return 0 everytime you hit wrong
        /// </summary>
        public int Streak { get; set; }

        /// <summary>
        /// Add a new gem to the game field. 2 type of gem that can be added: MovingGem or SustainedGem
        /// </summary>
        /// <param name="type">Type of Gem</param>
        /// <param name="newx">X Coordinate of added gem</param>
        /// <param name="newy">Y Coordinate of added gem</param>
        public void AddGem(GemType type,double newx,double newy)
        {
            if (type == GemType.MovingGem)
            {
                this.MovingGems.Add(new MovingGem(newx, newy));
            }
            else if (type == GemType.SustainedGem)
            {
                this.SustainedGems.Add(new SustainedGem(newx, newy));
            }
        }
    }
}
