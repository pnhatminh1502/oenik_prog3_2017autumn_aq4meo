﻿// <copyright file="MovingGem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace GuitarHeroClone
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// MovingGem is ellipse gem that moving from the top down to the end of game field
    /// To achieve point and streak you must press it on time and on the right color
    /// </summary>
    public class MovingGem : Gem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MovingGem"/> class.
        /// </summary>
        /// <param name="newx">X Coordinator of Sustained Gem</param>
        /// <param name="newy">Y Coordinator of Sustained Gem</param>
        public MovingGem(double newx, double newy)
            : base(newx, newy)
        {
            this.Geometry = new EllipseGeometry(this.Point, this.Radius, this.Radius);
            this.DY = 2;
            this.Type = GemType.StaticGem;
        }

        /// <summary>
        /// Gets or sets of the Moving Gem geometry. It's a Ellipse Geometry
        /// </summary>
        public EllipseGeometry Geometry { get; set; }

        /// <summary>
        /// Moving the Gem by changing the Point center's y coordinates
        /// </summary>
        public void ChangeY()
        {
            this.Geometry.Center = new Point(this.Geometry.Center.X, this.Geometry.Center.Y + this.DY);
        }
    }
}
