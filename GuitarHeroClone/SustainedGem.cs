﻿// <copyright file="SustainedGem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace GuitarHeroClone
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Sustained Gem is the gem that can be prolong. To achieve maximum point you must hold it as long as it passes the StaticGem
    /// </summary>
    public class SustainedGem : Gem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SustainedGem"/> class.
        /// </summary>
        /// <param name="newx">X Coordinator of Sustained Gem</param>
        /// <param name="newy">Y Coordinator of Sustained Gem</param>
        public SustainedGem(double newx, double newy)
            : base(newx, newy)
        {
            this.Geometry = new RectangleGeometry(new Rect(this.Point.X - this.Radius, this.Point.Y, this.Radius, this.Radius));
            this.DY = 2;
        }

        /// <summary>
        /// Gets or sets of the Sustained Gem geometry. It's a rectangle that can be connected by another rectangle
        /// </summary>
        public RectangleGeometry Geometry { get; set; }

        /// <summary>
        /// Moving the Sustaiend Gem by changing its Rect.Y coordinator
        /// </summary>
        public void ChangeY()
        {
            this.Geometry.Rect = new Rect(this.Geometry.Rect.X, this.Geometry.Rect.Y + this.DY, this.Radius * 2, this.Radius * 2);
        }
    }
}
