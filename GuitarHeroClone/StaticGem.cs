﻿// <copyright file="StaticGem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace GuitarHeroClone
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Static Gems are those staying at the bottom of the game field. We can control each Gem.
    /// There're in total 6 static gems with 6 different colors
    /// </summary>
    public class StaticGem : Gem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StaticGem"/> class.
        /// </summary>
        /// <param name="newx">X Coordinates of Static Gem</param>
        /// <param name="newy">Y Coordinates of Static Gem</param>
        public StaticGem(double newx, double newy)
            : base(newx, newy)
        {
            this.Geometry = new EllipseGeometry(this.Point, this.Radius, this.Radius);
            this.DY = 0;
            this.KeyDown = false;
        }

        /// <summary>
        /// Gets or sets ellipse Geometry of the Gem
        /// </summary>
        public EllipseGeometry Geometry { get; set; }

        /// <summary>
        /// Gem border color would be changed if we press the responsive button. 
        /// It helps adding the feeling of pressing a button
        /// </summary>
        /// <returns>
        /// Gem's border color
        /// If the gem is pressed down then its border turn to its inside color. Otherwise the border is Black
        /// </returns>
        public Pen GetPen()
        {
            return this.KeyDown ? new Pen(this.GetColor(), 5) : new Pen(Brushes.Black, 5);
        }
    }
}
