var indexSectionsWithContent =
{
  0: "acdghikmprstwx",
  1: "acgmps",
  2: "gx",
  3: "acgims",
  4: "ghw",
  5: "g",
  6: "dgkmprst"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "properties"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Properties"
};

