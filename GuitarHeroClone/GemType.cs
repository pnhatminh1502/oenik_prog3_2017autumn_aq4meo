﻿// <copyright file="GemType.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace GuitarHeroClone
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// EnumType of gem: StaticGem, MovingGem, SustainedGem
    /// </summary>
    public enum GemType
    {
        StaticGem, MovingGem, SustainedGem
    }
}
