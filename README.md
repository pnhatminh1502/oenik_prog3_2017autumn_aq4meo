Game project for Software Engineering I at University.  
The requirement is to implement a simple with C# WPF.  
The game is a simple clone of the notorious Guitar Hero.  
Basically there would be Gem dropping down from top to bottom as the song is progressing and the player has to hit the gem at the given node mimicking 
the guitar string at the correct time to score.  
There are two different Gem: Square game denoting a single note and a Bar gem denoting prolonged note requiring the player to hold down the button.  